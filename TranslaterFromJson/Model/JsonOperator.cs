﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Documents;

namespace TranslaterFromJson.Model;

public class JsonOperator
{
    string filePath;

    public string FilePath { get => filePath; set => filePath = value; }
    public int CurrentNum { get => currentNum; set => currentNum = value; }
    public List<JsonStruct> JsonDataset { get => jsonDataset; set => jsonDataset = value; }
    public List<string> AllTypes { get => allTypes; set => allTypes = value; }

    List<JsonStruct> jsonDataset;
    List<string> allTypes = new List<string>();

    int currentNum =0;

    public JsonOperator(string filePath)
    {
        try
        {
            JObject json = JObject.Parse(File.ReadAllText(filePath));
            FilePath = filePath;
            JsonDataset = new List<JsonStruct>();

            AllTypes=new List<string>();

            foreach (JProperty item in json.Properties())
            {
                JsonStruct oneItem = new JsonStruct();
                oneItem.ItemName = item.Name;
                foreach (JProperty lang in item.Value.Children<JProperty>())
                {
                    LanguageItem languageItem = new LanguageItem();
                    languageItem.LangName = lang.Name;
                    languageItem.LangValue = lang.Value.ToString();

                    if (!AllTypes.Contains(languageItem.LangName))
                    {
                        AllTypes.Add(languageItem.LangName);
                    }

                    oneItem.ItemNode.Add(languageItem);
                }
                JsonDataset.Add(oneItem);
            }
        }
        catch (System.Exception)
        {

            //不能解析的json
        }
        

    }

    public int getCurrentNum()
    {
        return currentNum;
    }
    public void setCurrentNum(int num)
    {
        currentNum = num;
    }
    public JsonStruct getNext()
    {
        CurrentNum++;
        if (CurrentNum < JsonDataset.Count)
        {
            return JsonDataset[CurrentNum];
        }
        else
        {
            if (CurrentNum >= JsonDataset.Count - 1) CurrentNum = JsonDataset.Count - 1;
            return JsonDataset.Last();
        }
    }
    public JsonStruct getLast()
    {
        CurrentNum--;
        if (CurrentNum >=0)
        {
            return JsonDataset[CurrentNum];
        }
        else { return JsonDataset[0]; }
    }
    public JsonStruct getNextNotTrans(string langType)
    {
        CurrentNum++;
        while(CurrentNum < JsonDataset.Count)
        {
            if (!isContansName(langType, JsonDataset[CurrentNum].ItemNode))
            {
                return JsonDataset[CurrentNum];
            }
            else { CurrentNum++; }
        }
        if(CurrentNum>=JsonDataset.Count-1) CurrentNum=JsonDataset.Count-1;
        return null;
    }
    public int getNextNotTransIndex(string langType)
    {
        currentNum++;
        while (currentNum < JsonDataset.Count)
        {
            if (!isContansName(langType, JsonDataset[currentNum].ItemNode))
            {
                return currentNum;
            }
            else { currentNum++; }
        }
        if (currentNum >= JsonDataset.Count - 1) currentNum = JsonDataset.Count - 1;
        return -1;
    }

    public bool isContansName(string name,List<LanguageItem> langItem)
    {
        for (int i = 0; i < langItem.Count; i++)
        {
            if (langItem[i].LangName==name)
            {
                return true;
            }
        }
        return false;
    }

    public JsonStruct getItemFromIndex(int index)
    {
        if (index < JsonDataset.Count && index >= 0) { return JsonDataset[index]; }
        else if (index >= JsonDataset.Count) { return JsonDataset.Last(); }
        else { return JsonDataset[0]; }
    }

    public void save()
    {
        string[] filename= Path.GetFileName(FilePath).Split('.');
        string saveName = filename[0] + "_save." + filename[1];
        saveName=Path.Combine(Path.GetDirectoryName(FilePath), saveName);

        Dictionary<string, Dictionary<string, string>> tempDateset = saveDictionary();
        string json=JsonConvert.SerializeObject(tempDateset,Formatting.Indented);

        File.WriteAllText(saveName,json);
    }
    private Dictionary<string,Dictionary<string,string>> saveDictionary()
    {
        Dictionary<string, Dictionary<string, string>> result = new Dictionary<string, Dictionary<string, string>>();
        for (int i = 0; i < jsonDataset.Count; i++)
        {
            Dictionary<string, string> subNode = new Dictionary<string, string>();
            for (int j = 0; j < jsonDataset[i].ItemNode.Count; j++)
            {
                subNode.Add(jsonDataset[i].ItemNode[j].LangName, jsonDataset[i].ItemNode[j].LangValue);
            }
            result.Add(jsonDataset[i].ItemName,subNode);
        }
        return result;
    }
}
