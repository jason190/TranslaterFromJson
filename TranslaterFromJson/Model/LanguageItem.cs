﻿namespace TranslaterFromJson.Model;

public class LanguageItem
{
    string langName;
    string langValue;

    public string LangName { get => langName; set => langName = value; }
    public string LangValue { get => langValue; set => langValue = value; }
}
