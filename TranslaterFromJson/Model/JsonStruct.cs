﻿using System.Collections.Generic;
using System.Windows.Documents;

namespace TranslaterFromJson.Model;

public class JsonStruct
{
    string itemName;
    List<LanguageItem> itemNode=new List<LanguageItem>();

    public string ItemName { get => itemName; set => itemName = value; }
    public List<LanguageItem> ItemNode { get => itemNode; set => itemNode = value; }
}
