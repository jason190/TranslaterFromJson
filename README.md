# TranslaterFromJson
我就不信有人能看我项目

#### 介绍
使用json格式存储的多国语言包的翻译时用的工具

#### 软件架构
json格式是固定的，更改层级关系读取不了
使用了view viewmodel model模式，但是没有添加serverlocator，本来设计了弹出窗体添加翻译字段名，没有servicelocator没法正确传参；
后来又尝试过使用同一个viewmodel，但是弹出窗体关不上，获得不到全局的弹出窗体实例，关闭时提示实例是null。怎么在viewmodel里关闭窗体？
viewmodel中取到的model实例属性与实例里面的属性不一致，牺牲性能（动态计算一次）得到的属性


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
